﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityNative.Toasts.Example;

public class ServiceViewer : MonoBehaviour
{
    [SerializeField] private Button buttonStart;
    [SerializeField] private Button buttonStop;
    [SerializeField] private Button buttonBack;

    private Coroutine _serviceCoroutine;
    
    private void Awake()
    {
        buttonStart.onClick.AddListener(ButtonStartOnClick);
        buttonStop.onClick.AddListener(ButtonStopOnClick);
        buttonBack.onClick.AddListener(ButtonBackOnClick);
    }

    private void ButtonBackOnClick()
    {
        gameObject.SetActive(false);
    }

    private void ButtonStopOnClick()
    {
        if (_serviceCoroutine != null)
        {
            StopCoroutine(_serviceCoroutine);
            _serviceCoroutine = null;
        }
    }

    private void ButtonStartOnClick()
    {
        if (_serviceCoroutine == null)
        {
            _serviceCoroutine = StartCoroutine(ServiceCoroutine());
        }
    }

    private IEnumerator ServiceCoroutine()
    {
        var counter = 0;
        while (true)
        {
            yield return new WaitForSeconds(4);
            counter++;
            ShowToast($"Current counter is {counter}");
        }
    }

    protected void ShowToast(string message)
    {
#if UNITY_ANDROID
        UnityNativeToastsHelper.ShowShortText(message);
#endif
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
